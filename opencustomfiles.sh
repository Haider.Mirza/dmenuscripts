#!/usr/bin/env bash

# This is a test script
# Variables

myDir='/home/haiderm/Documents/'

# giving dmenu a directory

options=$(cd ${myDir} && ls)
arr=($options)
choice=$(echo -e "${options[@]}" | dmenu -i -p 'My directories: ')

# If statments

if [ "$choice" == ${arr[0]} ]; then
    exec emacsclient -c ${myDir}${arr[0]}
elif [ "$choice" == ${myDir}${arr[1]} ]; then
    exec emacsclient -c ${myDir}${arr[1]}
elif [ "$choice" == ${arr[2]} ]; then
    exec emacsclient -c ${myDir}${arr[2]}
elif [ "$choice" == ${arr[3]} ]; then
    exec emacsclient -c ${myDir}${arr[3]}
fi
